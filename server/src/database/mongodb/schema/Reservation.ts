const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReservationSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    hotelName: {
        type: String,
        required: true
    },
    guestName: {
        type: String,
        required: true
    },
    arrivalDate: {
        type: Date,
        required: true
    },
    departureDate: {
        type: Date,
        required: true
    }
});

export const Reservation = mongoose.model("Reservation", ReservationSchema);
