import {logger} from "../../utils/logger";

const mongoose = require('mongoose');

export async function init(db: string) {
    return mongoose
        .connect(
            db,
            {useCreateIndex: true, useNewUrlParser: true, useFindAndModify: false}
        )
        .then(() => logger.info("MongoDB connected"))
}

