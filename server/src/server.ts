import {schema} from "./graphql/schema";
import * as express from "express";
import * as graphqlHTTP from "express-graphql";
import * as mongodb from "./database/mongodb/db";
import {logger} from "./utils/logger";

let cors = require('cors');
let config = require("../config");
let port = config.port ? config.port : 3000;
mongodb
    .init(config.mongodb)
    .then(
        () => {
            const app = express();
            const endpoint = '/graphql';

            app.use(cors());
            app.use(endpoint, graphqlHTTP({
                schema: schema,
                // rootValue: root,
                graphiql: true //Set to false if you don't want graphiql enabled
            }));

            app.listen(port);
            logger.info(`GraphQL API server running at http://localhost:${port}${endpoint}`);
        }
    )
    .catch(
        err => {
            logger.error(`failed to initial server, err:${JSON.stringify(err)} (forget set current ip for mongodb whitelist?)`);
        }
    );
