import {query} from './query';
import {mutation} from './mutation';
import {GraphQLSchema} from "graphql";


export let schema = new GraphQLSchema({
    query,
    mutation
});
