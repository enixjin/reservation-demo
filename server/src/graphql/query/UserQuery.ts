import {GraphQLList} from "graphql";
import {UserType} from "../type/UserType";
import {sendHttp} from "../../rest";

export let userQuery = () => ({
    users: {
        type: new GraphQLList(UserType),
        description: "get user list from rest api",
        resolve: () => {
            return sendHttp("jsonplaceholder.typicode.com", 80, "/users", "GET", "").then(JSON.parse);
        }
    },
});
