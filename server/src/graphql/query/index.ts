import {reservationQuery} from "./ReservationQuery";
import {GraphQLObjectType} from "graphql";
import {userQuery} from "./UserQuery";

export const query = new GraphQLObjectType({
    name: "query",
    description: "root query",
    fields: () => {
        return Object.assign({},
            reservationQuery(),
            userQuery());
    }
});
