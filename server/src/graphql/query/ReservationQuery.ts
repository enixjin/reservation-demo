import {ReservationType} from '../type/ReservationType';
import {GraphQLList, GraphQLNonNull, GraphQLString} from "graphql";
import {Reservation} from "../../database/mongodb/schema/Reservation";

export let reservationQuery = () => ({
    reservations: {
        type: new GraphQLList(ReservationType),
        description:"get reservation list from mongodb",
        resolve: () => {
            return new Promise((resolve, reject) => {
                Reservation.find({})
                    .exec((err, res) => {
                        err ? reject(err) : resolve(res);
                    });
            });
        }
    },
    reservation: {
        type: ReservationType,
        description:"query reservation by id",
        args: {
            "id": {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: (root, args) => {
            return new Promise((resolve, reject) => {
                Reservation.findOne(args)
                    .exec((err, res) => {
                        err ? reject(err) : resolve(res);
                    });
            });
        }
    }
});
