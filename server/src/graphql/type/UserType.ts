import {GraphQLInt, GraphQLNonNull, GraphQLObjectType, GraphQLString} from "graphql";

export const AddressType = new GraphQLObjectType({
    name: "address",
    fields: {
        street: {
            type: new GraphQLNonNull(GraphQLString),
            description: "street of address"
        },
        suite: {
            type: new GraphQLNonNull(GraphQLString),
            description: "suite of address"
        },
        city: {
            type: new GraphQLNonNull(GraphQLString),
            description: "city of address"
        },
        zipcode: {
            type: new GraphQLNonNull(GraphQLString),
            description: "zipcode of address"
        },
    }
});

export const UserType = new GraphQLObjectType({
    name: "user",
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLInt),
            description: "id of user"
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: "name of user"
        },
        username: {
            type: new GraphQLNonNull(GraphQLString),
            description: "user name of user"
        },
        email: {
            type: new GraphQLNonNull(GraphQLString),
            description: "email of user"
        },
        address: {
            type: new GraphQLNonNull(AddressType),
            description: "address of user"
        },
        phone: {
            type: new GraphQLNonNull(GraphQLString),
            description: "phone number of user"
        },
    }
});
