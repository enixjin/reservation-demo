import {GraphQLScalarType} from 'graphql';


const parse = (value) => {
    if (typeof value === "number") {
        return new Date(value);
    }
    throw new Error('invalid number for DateTime.');
};

const serialize = (value) => {
    if (value instanceof Date) {
        return value.getTime();
    }
    throw new Error('invalid date for DateTime.');
};

const parseLiteral = ({kind, value}) => {
    if (kind === "IntValue") {
        return new Date(+value);
    }
    throw new Error('invalid number for DateTime.');
};

const DateTime = new GraphQLScalarType({
    name: 'DateTime',
    description: 'An date number from date.getTime()',
    serialize: serialize,
    parseValue: parse,
    parseLiteral: parseLiteral,
});

export {DateTime as default};
