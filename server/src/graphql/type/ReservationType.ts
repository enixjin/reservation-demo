import {GraphQLInputObjectType, GraphQLInt, GraphQLNonNull, GraphQLObjectType, GraphQLString} from "graphql";
import DateTime from "./scalars/DateTime";

export const ReservationType = new GraphQLObjectType({
    name: "reservation",
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: "id of reservation"
        },
        guestName: {
            type: new GraphQLNonNull(GraphQLString),
            description: "guest name of reservation"
        },
        hotelName: {
            type: new GraphQLNonNull(GraphQLString),
            description: "hotel name of reservation"
        },
        arrivalDate: {
            type: new GraphQLNonNull(DateTime),
            description: "arrival date of reservation"
        },
        departureDate: {
            type: new GraphQLNonNull(DateTime),
            description: "departure date of reservation"
        },
    }
});

export const ReservationInputType = new GraphQLInputObjectType({
    name: "reservationInput",
    fields: {
        guestName: {
            type: GraphQLString,
            description: "guest name of reservation"
        },
        hotelName: {
            type: GraphQLString,
            description: "hotel name of reservation"
        },
        arrivalDate: {
            type: DateTime,
            description: "arrival date of reservation"
        },
        departureDate: {
            type: DateTime,
            description: "departure date of reservation"
        },
    }
});
