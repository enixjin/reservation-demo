import {ReservationInputType, ReservationType} from '../type/ReservationType';
import * as uuidv4 from 'uuid/v4';
import {Reservation} from '../../database/mongodb/schema/Reservation';
import {GraphQLNonNull, GraphQLString} from "graphql";
import {logger} from "../../utils/logger";

export let reservationMutation = () => ({
    addReservation: {
        type: ReservationType,
        description: "add new reservation",
        args: {newReservation: {type: ReservationInputType}},
        resolve: (root, {newReservation}) => {
            logger.info(`${JSON.stringify(newReservation)}`);
            newReservation.id = uuidv4();
            const newRecord = new Reservation(newReservation);
            return new Promise((resolve, reject) => {
                newRecord.save((err, res) => {
                    err ? reject(err) : resolve(res);
                });
            });
        }
    },
    updateReservation: {
        type: ReservationType,
        description: "update reservation by id",
        args: {
            id: {
                type: new GraphQLNonNull(GraphQLString)
            }, ...ReservationInputType.getFields()
        },
        resolve: (root, {id, ...update}) => {
            return new Promise((resolve, reject) => {
                Reservation.findOneAndUpdate({id}, {$set: update}, {new: true})
                    .exec(
                        (err, res) => {
                            err ? reject(err) : resolve(res);
                        }
                    );
            });
        }
    },
    deleteReservation: {
        type: ReservationType,
        description:"delete reservation by id",
        args: {
            id: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: (root, {id}) => {
            return new Promise((resolve, reject) => {
                Reservation.findOneAndDelete({id})
                    .exec(
                        (err, res) => {
                            err ? reject(err) : resolve(res);
                        }
                    );
            });
        }
    },
});
