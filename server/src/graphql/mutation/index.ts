import {GraphQLObjectType} from "graphql";
import {reservationMutation} from "./ReservationMutation";

export const mutation = new GraphQLObjectType({
    name: "mutation",
    description: "root mutation",
    fields: () => {
        return Object.assign({}, reservationMutation());
    }
});
