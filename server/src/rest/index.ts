import {logger} from "../utils/logger";
import * as http from "http";

export function sendHttp(hostname: string, port: number, path: string, method: string, postData: string, contentType = "application/json"): Promise<any> {
    return new Promise((resolve, reject) => {
        const options = {
            hostname,
            port,
            path: encodeURI(path),
            method: method,
            headers: {
                "Content-Type": contentType
            },
        };
        logger.silly(`sending request to ${options.path}, method: ${options.method}`);
        const req = http.request(options, res => {
            let result = "";
            res.on('data', d => {
                result += d;
            });
            res.on('end', _ => {
                try {
                    if (res.statusCode >= 200 && res.statusCode < 300) {
                        resolve(result);
                    } else {
                        let jsonResult = JSON.parse(result);
                        reject(jsonResult);
                    }
                } catch (e) {
                    logger.error(`fail to parse, statusCode:${res.statusCode} result:${result}`);
                    reject(`fail to parse result`);
                }
            })
        });

        req.on('error', (e) => {
            logger.error(e);
            reject(e);
        });

        if (postData) {
            logger.silly(`writing body: ${postData}`);
            req.write(postData);
        }

        req.end();
    });
}
