# Demo Reservation System

## Tech stack:

   * NodeJS >= 8.12
   * Reactjs
   * Material-UI
   * GraphQL
   * Typescript
   * Docker

## Projects:

   * server - express + graphql + mongodb
   * web - react + Material-UI + apollo client
