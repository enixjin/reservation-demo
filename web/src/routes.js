import React from 'react'
import {HashRouter, Route, Switch} from 'react-router-dom'
import ScrollToTop from './components/ScrollTop'
import ReservationList from './components/List';
import CreateReservation from "./components/CreateReservation";

export default props => (
    <HashRouter>
        <ScrollToTop>
            <Switch>
                <Route exact path='/' component={ReservationList}/>
                <Route exact path='/create' component={CreateReservation}/>
                <Route exact path='/query' component={CreateReservation}/>
            </Switch>
        </ScrollToTop>
    </HashRouter>
)
