import {gql} from "apollo-boost";

export const ADD_RESERVATION = gql`mutation($data:reservationInput!) {
  addReservation(newReservation:$data){
    id
    guestName
    arrivalDate
    departureDate
    hotelName
  }
}`;

export const DELETE_RESERVATION = gql`mutation ($id: String!) {
  deleteReservation(id: $id) {
    id
    guestName
    arrivalDate
    departureDate
    hotelName
  }
}`;

export const LIST_RESERVATIONS = gql`
      {
        reservations{
            id
            guestName
            arrivalDate
            departureDate
            hotelName
        }
      }
    `;

export const QUERY_RESERVATION_BY_ID = gql`query ($id: String!) {
  reservation(id: $id) {
    id
    guestName
    arrivalDate
    departureDate
    hotelName
  }
}`;
