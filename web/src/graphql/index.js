import ApolloClient from "apollo-boost";

export const client = new ApolloClient({
    uri: "https://reservation-demo-gql.enixjin.net/graphql"
});

export * from "./Reservation";
