import React, {Component} from 'react';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import './App.css';
import Routes from './routes';
import {blue, indigo} from '@material-ui/core/colors';
import {ApolloProvider} from "react-apollo";
import {client} from "./graphql";

const theme = createMuiTheme({
    palette: {
        secondary: {
            main: blue[900]
        },
        primary: {
            main: indigo[700]
        }
    },
    typography: {
        useNextVariants: true,
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '"Lato"',
            'sans-serif'
        ].join(',')
    }
});


class App extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <div>
                    <MuiThemeProvider theme={theme}>
                        <Routes/>
                    </MuiThemeProvider>
                </div>
            </ApolloProvider>
        );
    }
}

export default App;
