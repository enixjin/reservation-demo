import React, {Component} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {withRouter} from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import Back from './common/Back';
import TextField from '@material-ui/core/TextField';
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import {DatePicker, MuiPickersUtilsProvider} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import {ADD_RESERVATION, client, LIST_RESERVATIONS} from "../graphql";

const backgroundShape = require('../images/shape.svg');

const logo = require('../images/logo.svg');

const numeral = require('numeral');
numeral.defaultFormat('0');

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.secondary['A100'],
        overflow: 'hidden',
        background: `url(${backgroundShape}) no-repeat`,
        backgroundSize: 'cover',
        backgroundPosition: '0 400px',
        marginTop: 10,
        padding: 20,
        paddingBottom: 500
    },
    grid: {
        margin: `0 ${theme.spacing.unit * 2}px`
    },
    smallContainer: {
        width: '60%'
    },
    bigContainer: {
        width: '80%'
    },
    logo: {
        marginBottom: 24,
        display: 'flex',
        justifyContent: 'center'
    },
    stepContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    stepGrid: {
        width: '80%'
    },
    buttonBar: {
        marginTop: 32,
        display: 'flex',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: theme.palette.primary['A100']
    },
    backButton: {
        marginRight: theme.spacing.unit,
    },
    outlinedButtom: {
        textTransform: 'uppercase',
        margin: theme.spacing.unit
    },
    stepper: {
        backgroundColor: 'transparent'
    },
    paper: {
        padding: theme.spacing.unit * 3,
        textAlign: 'left',
        color: theme.palette.text.secondary
    },
    topInfo: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 42
    },
    formControl: {
        width: '100%'
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    }
});

const getSteps = () => {
    return [
        'GuestInfo',
        'Hotel and Time',
        'Confirm'
    ];
};

class CreateReservation extends Component {

    state = {
        activeStep: 0,
        reservation: {guestName: "", hotelName: "", arrivalDate: new Date(), departureDate: new Date()},
        termsChecked: false,
        loading: true,
        labelWidth: 0,
        reservationID: ""
    };

    componentDidMount() {

    }

    handleNext = () => {
        this.setState(state => ({
            activeStep: state.activeStep + 1,
        }));
        if (this.state.activeStep === 2) {
            let data = Object.assign({}, this.state.reservation);
            data.departureDate = data.departureDate.getTime();
            data.arrivalDate = data.arrivalDate.getTime();
            client.mutate({
                mutation: ADD_RESERVATION
                , variables: {data}
                , update: (cache, {data: {addReservation}}) => {
                    try {
                        const {reservations} = cache.readQuery({query: LIST_RESERVATIONS});
                        cache.writeQuery({
                            query: LIST_RESERVATIONS,
                            data: {reservations: reservations.concat([addReservation])},
                        });
                    } catch (e) {
                        // query not in cache, do nothing.
                    }
                    this.setState({reservationID: addReservation.id});
                    this.setState({loading: false});
                }
            })
        }
    };

    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1,
        }));
    };

    handleChange = event => {
        let newState = Object.assign({}, this.state);
        newState.reservation[event.target.name] = event.target.value;
        this.setState(newState);
    };


    stepActions() {
        if (this.state.activeStep === 2) {
            return 'Accept';
        }
        return 'Next';
    }

    render() {

        const {classes} = this.props;
        const steps = getSteps();
        const {activeStep, loading, reservationID} = this.state;

        return (
            <React.Fragment>
                <CssBaseline/>
                <div className={classes.root}>
                    <Back/>
                    <Grid container justify="center">
                        <Grid spacing={24} alignItems="center" justify="center" container className={classes.grid}>
                            <Grid item xs={12}>
                                <div className={classes.logo}>
                                    <img width={100} height={100} src={logo} alt=""/>
                                </div>
                                <div className={classes.stepContainer}>
                                    <div className={classes.stepGrid}>
                                        <Stepper classes={{root: classes.stepper}} activeStep={activeStep}
                                                 alternativeLabel>
                                            {steps.map(label => {
                                                return (
                                                    <Step key={label}>
                                                        <StepLabel>{label}</StepLabel>
                                                    </Step>
                                                );
                                            })}
                                        </Stepper>
                                    </div>
                                    {activeStep === 0 && (
                                        <div className={classes.smallContainer}>
                                            <Paper className={classes.paper}>
                                                <div>
                                                    <div style={{marginBottom: 32}}>
                                                        <Typography variant="subtitle1" style={{fontWeight: 'bold'}}
                                                                    gutterBottom>
                                                            Guest Information
                                                        </Typography>
                                                        <Typography variant="body1" gutterBottom>
                                                            Please fill guest information like name
                                                        </Typography>
                                                    </div>
                                                    <div>
                                                        <FormControl variant="outlined" className={classes.formControl}>
                                                            <TextField
                                                                required
                                                                id="outlined-required"
                                                                label="Guest Name"
                                                                margin="normal"
                                                                variant="outlined"
                                                                name="guestName"
                                                                value={this.state.reservation.guestName}
                                                                onChange={this.handleChange}
                                                            />
                                                        </FormControl>
                                                    </div>
                                                </div>
                                            </Paper>
                                        </div>
                                    )}
                                    {activeStep === 1 && (
                                        <div className={classes.smallContainer}>
                                            <Paper className={classes.paper}>
                                                <div>
                                                    <div style={{marginBottom: 32}}>
                                                        <Typography variant="subtitle1" style={{fontWeight: 'bold'}}
                                                                    gutterBottom>
                                                            Hotel and Time
                                                        </Typography>
                                                        <Typography variant="body1" gutterBottom>
                                                            Please select hotel and arrival/departure Date
                                                        </Typography>
                                                    </div>
                                                    <div>
                                                        <Grid container spacing={24}>
                                                            <Grid item xs={12}>
                                                                <Paper className={classes.paper}>
                                                                    <FormControl variant="outlined"
                                                                                 className={classes.formControl}>
                                                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                            <Grid container className={classes.grid}
                                                                                  justify="space-around">
                                                                                <DatePicker
                                                                                    disablePast
                                                                                    keyboard
                                                                                    variant="outlined"
                                                                                    label="Arrival Date"
                                                                                    format="yyyy-MM-dd"
                                                                                    value={this.state.reservation.arrivalDate ? this.state.reservation.arrivalDate : new Date()}
                                                                                    onChange={value => this.handleChange({
                                                                                        target: {
                                                                                            name: "arrivalDate",
                                                                                            value
                                                                                        }
                                                                                    })}
                                                                                />
                                                                                <DatePicker
                                                                                    disablePast
                                                                                    keyboard
                                                                                    variant="outlined"
                                                                                    minDate={this.state.reservation.arrivalDate}
                                                                                    label="Departure Date"
                                                                                    format="yyyy-MM-dd"
                                                                                    value={this.state.reservation.departureDate ? this.state.reservation.departureDate : new Date()}
                                                                                    onChange={value => this.handleChange({
                                                                                        target: {
                                                                                            name: "departureDate",
                                                                                            value
                                                                                        }
                                                                                    })}
                                                                                />
                                                                            </Grid>
                                                                        </MuiPickersUtilsProvider>
                                                                    </FormControl>
                                                                </Paper>
                                                            </Grid>
                                                            <Grid item xs={12}>
                                                                <Paper className={classes.paper}>
                                                                    <FormControl required
                                                                                 className={classes.formControl}>
                                                                        <InputLabel htmlFor="hotelName-native-required">Hotel
                                                                            Name</InputLabel>
                                                                        <Select
                                                                            native
                                                                            value={this.state.reservation.hotelName}
                                                                            onChange={this.handleChange}
                                                                            name="hotelName"
                                                                            inputProps={{
                                                                                id: 'hotelName-native-required',
                                                                            }}
                                                                        >
                                                                            <option value=""/>
                                                                            <option value="hotelA">Hotel A</option>
                                                                            <option value="hotelB">Hotel B</option>
                                                                            <option value="hotelC">Hotel C</option>
                                                                        </Select>
                                                                        <FormHelperText>Required</FormHelperText>
                                                                    </FormControl>
                                                                </Paper>
                                                            </Grid>
                                                            <Grid item xs={6}>
                                                            </Grid>
                                                        </Grid>
                                                    </div>
                                                </div>
                                            </Paper>
                                        </div>
                                    )}
                                    {activeStep === 2 && (
                                        <div className={classes.smallContainer}>
                                            <Paper className={classes.paper}>
                                                <div>
                                                    <div>
                                                        <Typography color='primary' gutterBottom>
                                                            Please confirm Following information:
                                                        </Typography>
                                                        <List component="nav">
                                                            <ListItem button>
                                                                <ListItemText
                                                                    primary={"Guest Name: " + this.state.reservation.guestName}/>
                                                            </ListItem>
                                                            <ListItem button>
                                                                <ListItemText
                                                                    primary={"Hotel Name: " + this.state.reservation.hotelName}/>
                                                            </ListItem>
                                                            <ListItem button>
                                                                <ListItemText
                                                                    primary={"Arrival Date: " + this.state.reservation.arrivalDate.toJSON().slice(0, 10)}/>
                                                            </ListItem>
                                                            <ListItem button>
                                                                <ListItemText
                                                                    primary={"Departure Date: " + this.state.reservation.departureDate.toJSON().slice(0, 10)}/>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                </div>
                                            </Paper>
                                        </div>
                                    )}
                                    {activeStep === 3 && (
                                        <div className={classes.bigContainer}>
                                            <Paper className={classes.paper}>
                                                <div style={{display: 'flex', justifyContent: 'center'}}>
                                                    {reservationID && (
                                                        <div style={{marginBottom: 32, textAlign: 'center'}}>
                                                            <Typography variant="h5" style={{fontWeight: 'bold'}}>
                                                                Your reservation ID is : {reservationID}
                                                            </Typography>
                                                        </div>)}
                                                    {!reservationID && (<div style={{width: 380, textAlign: 'center'}}>
                                                        <div style={{marginBottom: 32}}>
                                                            <Typography variant="h6" style={{fontWeight: 'bold'}}
                                                                        gutterBottom>
                                                                Processing your request
                                                            </Typography>
                                                            <Typography variant="body1" gutterBottom>
                                                                Please not close this window
                                                            </Typography>
                                                        </div>
                                                        <div>
                                                            <Fade
                                                                in={loading}
                                                                style={{
                                                                    transitionDelay: loading ? '800ms' : '0ms',
                                                                }}
                                                                unmountOnExit
                                                            >
                                                                <CircularProgress style={{
                                                                    marginBottom: 32,
                                                                    width: 100,
                                                                    height: 100
                                                                }}/>
                                                            </Fade>
                                                        </div>
                                                    </div>)}
                                                </div>
                                            </Paper>
                                        </div>
                                    )}
                                    {activeStep !== 3 && (
                                        <div className={classes.buttonBar}>
                                            {activeStep !== 2 ? (
                                                <Button
                                                    disabled={activeStep === 0}
                                                    onClick={this.handleBack}
                                                    className={classes.backButton}
                                                    size='large'
                                                >
                                                    Back
                                                </Button>
                                            ) : (
                                                <Button
                                                    disabled={activeStep === 0}
                                                    onClick={this.handleBack}
                                                    className={classes.backButton}
                                                    size='large'
                                                >
                                                    Cancel
                                                </Button>
                                            )}
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                onClick={this.handleNext}
                                                size='large'
                                                style={this.state.reservation.guestName ? {
                                                    background: classes.button,
                                                    color: 'white'
                                                } : {}}
                                                disabled={(!this.state.reservation.guestName && activeStep === 0) || (!this.state.reservation.hotelName && activeStep === 1)}
                                            >
                                                {this.stepActions()}
                                            </Button>
                                        </div>
                                    )}

                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
        )
    }
}

export default withRouter(withStyles(styles)(CreateReservation))
