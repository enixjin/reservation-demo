const Menu = [
    {
        label: "Reservation List",
        pathname: "/"
    },
    {
        label: "Make a Reservation",
        pathname: "/create"
    },
];

export default Menu;
