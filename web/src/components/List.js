import React, {Component} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Topbar from './Topbar';
import SectionHeader from './typo/SectionHeader';
import ReservationItem from './reservations/ReservationItems';
import TextField from "@material-ui/core/TextField";
import {LIST_RESERVATIONS, QUERY_RESERVATION_BY_ID} from "../graphql";
import {Query} from "react-apollo";

const backgroundShape = require('../images/shape.svg');

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.grey['A500'],
        overflow: 'hidden',
        background: `url(${backgroundShape}) no-repeat`,
        backgroundSize: 'cover',
        backgroundPosition: '0 400px',
        marginTop: 20,
        padding: 20,
        paddingBottom: 200
    },
    grid: {
        width: 1000
    }
});

class ReservationList extends Component {

    state = {
        searchID: ""
    };

    handleChange = event => {
        this.setState({searchID: event.target.value});
    };

    render() {
        const {classes} = this.props;
        const currentPath = this.props.location.pathname;

        return (
            <React.Fragment>
                <CssBaseline/>
                <Topbar currentPath={currentPath}/>
                <div className={classes.root}>
                    <Grid container justify="center">
                        <Grid spacing={24} alignItems="center" justify="center" container className={classes.grid}>
                            <Grid item xs={12}>
                                <SectionHeader title="Reservation List"/>
                                <TextField
                                    id="outlined-required"
                                    label="Search by reservation id"
                                    margin="normal"
                                    variant="outlined"
                                    name="guestName"
                                    value={this.state.searchID}
                                    fullWidth
                                    onChange={this.handleChange}
                                />
                                {this.state.searchID === "" && <Query
                                    query={LIST_RESERVATIONS}
                                >
                                    {({loading, error, data}) => {
                                        if (loading) return <React.Fragment>Loading...</React.Fragment>;
                                        if (error) return <React.Fragment>Error :(</React.Fragment>;
                                        return <ReservationItem reservations={data.reservations}/>;
                                    }}
                                </Query>}
                                {this.state.searchID !== "" && <Query
                                    query={QUERY_RESERVATION_BY_ID} variables={{id: this.state.searchID}}
                                >
                                    {({loading, error, data}) => {
                                        if (loading) return <React.Fragment>Loading...</React.Fragment>;
                                        if (error) return <React.Fragment>Error :(</React.Fragment>;
                                        if (!data.reservation) return <React.Fragment>No Matched Result!</React.Fragment>;
                                        return <ReservationItem reservations={[data.reservation]}/>;
                                    }}
                                </Query>}
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(ReservationList);
